[![Licence](https://img.shields.io/badge/license-CC%20BY--NC--ND%204.0-blue.svg)](LICENSE)

## A propos du projet

Le taquin est un jeu sur damier consistant à remettre une suite des lettres mélangées dans l'ordre alphabétique.
La subtilité du jeu réside dans le fait qu'il manque une case sur le damier, ce qui ne permet de bouger que les cases à proximité de ce trou.
Le jeu est divisé en trois niveaux de difficulté correspondant à des tailles de damiers de 3x3, 4x4 ou 5x5.

Pour installer le projet :

1. Ouvrez Unity
2. Compilez les sources
3. Testez sur l'émulateur ou un appareil branché par câble USB

Pour voir mes autres projets, rendez vous sur mon [portfolio](https://raphael-octau.fr/en).

## TODOs

- [ ] Améliorer la fluidité des déplacements

## Licence

&copy; 2019 Raphaël OCTAU

Ce travail est sous licence [Creative Commons (Attribution - Pas d'Utilisation Commerciale - Pas de Modification) 4.0 International](LICENSE).