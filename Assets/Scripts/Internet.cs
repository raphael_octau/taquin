﻿using UnityEngine;
using System.Collections;
using System ;

public class Internet : MonoBehaviour {
	private const bool allowCarrierDataNetwork = false;
	private const string pingAddress = "8.8.8.8"; // Google Public DNS server
	private const float waitingTime = 2.0f;

	private Ping ping;
	private float pingStartTime;

	//public Text textInternet;
	public GameObject MAJ;

	public string url = "http://aoctau.free.fr/taquin/version.txt";
	public string urlDownload = "http://aoctau.free.fr/taquin/lien.txt";//A changer aussi dans pub.cs
	float version = 1.0f;
	//changer la version dans TexteMAJ aussi!!!!!!

	// Use this for initialization
	void Start () {
		if (Application.platform != RuntimePlatform.Android)
			return;

		bool internetPossiblyAvailable;
		switch (Application.internetReachability)
		{
		case NetworkReachability.ReachableViaLocalAreaNetwork:
			internetPossiblyAvailable = true;
			break;
		case NetworkReachability.ReachableViaCarrierDataNetwork:
			internetPossiblyAvailable = allowCarrierDataNetwork;
			break;
		default:
			internetPossiblyAvailable = false;
			break;
		}
		if (!internetPossiblyAvailable)
		{
			InternetIsNotAvailable();
			return;
		}
		ping = new Ping(pingAddress);
		pingStartTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
		if (ping != null)
		{
			bool stopCheck = true;
			if (ping.isDone)
				InternetAvailable();
			else if (Time.time - pingStartTime < waitingTime)
				stopCheck = false;
			else
				InternetIsNotAvailable();
			if (stopCheck)
				ping = null;
		}
	}

	private void InternetIsNotAvailable()
	{
		Debug.Log("No Internet");
		/*textInternet.enabled = true;
		textInternet.GetComponent<BoxCollider2D>().enabled = true;*/
	}
	
	private void InternetAvailable()
	{
		Debug.Log("Internet available");
		/*textInternet.enabled = false;
		textInternet.GetComponent<BoxCollider2D>().enabled = false;*/
		StartCoroutine(Check());
	}

	IEnumerator Check() {
		WWW www = new WWW (url);
		WWW www2 = new WWW (urlDownload);
		yield return www;
		yield return www2;
		float NV;
		float.TryParse (www.text, out NV);

		if (NV > version) {
			Debug.Log ("Maj available");

			MAJ.SetActive(true);

			PlayerPrefs.SetString ("Lien", www2.text);
			PlayerPrefs.SetFloat ("Version", NV);
//			Application.LoadLevel ("MAJ");
		} else {
			Debug.Log ("No MAJ");
			MAJ.SetActive(false);
		}
	}
}
