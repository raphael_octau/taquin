﻿using UnityEngine;
using System.Collections;

public class DownloadUpdate : MonoBehaviour {

	private Vector3 taille;

	// Use this for initialization
	void Start () {
		taille = this.transform.localScale;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnMouseDown ()
	{
		this.transform.localScale = new Vector3 (taille.x / 1.1f, taille.y / 1.1f, taille.z / 1.1f);
	}
	
	void OnMouseExit ()
	{
		this.transform.localScale = new Vector3 (taille.x, taille.y, taille.z);
	}

	void OnMouseUpAsButton (){
		Application.OpenURL (PlayerPrefs.GetString ("Lien").ToString ());

		this.transform.localScale = new Vector3 (taille.x, taille.y, taille.z);
	}
}
