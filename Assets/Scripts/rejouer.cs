﻿using UnityEngine;
using System.Collections;

public class rejouer : MonoBehaviour {

	private Vector3 taille;

	// Use this for initialization
	void Start () {
		taille = this.transform.localScale;
	}

    void OnMouseDown()
    {
        this.transform.localScale = new Vector3(taille.x / 1.1f, taille.y / 1.1f, taille.z / 1.1f);
    }


    void OnMouseExit()
    {
        this.transform.localScale = new Vector3(taille.x, taille.y, taille.z);
    }

    void OnMouseUpAsButton () {
        this.transform.localScale = new Vector3(taille.x, taille.y, taille.z);

        if (this.name.Equals("Menu"))
		    Application.LoadLevel("Menu");
        else if (this.name.Equals("Rejouer"))
            Application.LoadLevel("load");
    }
}
