﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class controller : MonoBehaviour {

	public Text         win;
	public GameObject   right;
	public GameObject   left;
	public GameObject   up;
	public GameObject   down;
	public Text         nbDepl;
    public Text         record;
    public GameObject[] tuilesA;
    public GameObject[] tuilesB;
    public GameObject[] tuilesC;

    private int     ligneCaseVide = -1;
	private int     colCaseVide   = -1;
	private bool    bRandomized   = false;
    private bool    bGagne        = false;
	private char[,] tab;
    private char[,] tabA = new char[,]{ { 'F', 'L', 'B', 'H' },
                                        { 'N', 'G', ' ', 'D' },
                                        { 'A', 'I', 'E', 'J' },
                                        { 'C', 'K', 'M', 'O' } };

    private char[,] tabB = new char[,]{ { 'K', 'H', 'Q', 'L', 'R' },
                                        { 'U', 'F', 'V', 'G', 'T' },
                                        { 'X', 'P', ' ', 'O', 'I' },
                                        { 'A', 'S', 'W', 'C', 'M' },
                                        { 'D', 'J', 'B', 'N', 'E' } };

    private char[,] tabC = new char[,]{ { 'B', 'H', 'A' },
                                        { ' ', 'F', 'D' },
                                        { 'E', 'C', 'G' } };

    // Use this for initialization
    private void Start () {
        //PlayerPrefs.SetString("mode", "A");
		initialisation ();
		PlayerPrefs.SetInt ("nbDepl", 0);
		afficherTableau();
		randomization ();
	}
	
	// Update is called once per frame
	private void Update () {
        if (!bGagne && bRandomized && !verification ()) {
            if(PlayerPrefs.GetString("commandes") == "click")
            {
                if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
                {
                    if (Input.touchCount > 0 && Input.touchCount < 2)
                    {
                        if (Input.GetTouch(0).phase == TouchPhase.Began)
                        {
                            checkTouch(Input.GetTouch(0).position);
                        }
                    }
                }
                else if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.OSXEditor)
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        checkTouch(Input.mousePosition);
                    }
                }
            }
            
            if(PlayerPrefs.GetString("direction", "Z") != "Z")
            {
                deplacer(PlayerPrefs.GetString("direction", "Z"));
                PlayerPrefs.SetString("direction", "Z");
            }
		}
		nbDepl.GetComponent<Text>().text = "Déplacements\n" + PlayerPrefs.GetInt ("nbDepl");
	}

    private void checkTouch(Vector3 pos)
    {
        Vector3 wp = Camera.main.ScreenToWorldPoint(pos);
        Vector2 touchPos = new Vector2(wp.x, wp.y);
        RaycastHit2D hit = Physics2D.Raycast(touchPos, Vector2.zero);
        string posCollider = hit.collider.name.Substring(hit.collider.name.Length - 2);

        Debug.Log(hit.collider.name);
        if(hit.collider.name.Contains("Tuile"))
            verifPossible(int.Parse(posCollider[1].ToString()), int.Parse(posCollider[0].ToString()));
    }

    private void verifPossible(int i, int j)
    {
        if (j>0 && tab[i,j - 1] == ' ') { deplacer("E"); }
        if (j<tab.GetLength(0)-1 && tab[i,j + 1] == ' ') { deplacer("O"); }
        if (i>0 && tab[i - 1,j] == ' ') { deplacer("S"); }
        if (i<tab.GetLength(0)-1 && tab[i + 1,j] == ' ') { deplacer("N"); }
    }

    private void initialisation(){
        if (PlayerPrefs.GetString ("mode") == "A") {
            tab = new char[tabA.GetLength(0), tabA.GetLength(1)];
			for (int ligne = 0;ligne < tabA.GetLength(0); ligne++ )
				for (int colonne = 0;colonne < tabA.GetLength(1); colonne++ )
					tab[colonne,ligne] = tabA[colonne,ligne];
            //On detruit les tuiles des autres modes
            for (int i = 0; i < tuilesB.GetLength(0); i++)
                Destroy(tuilesB[i]);
            for (int i = 0; i < tuilesC.GetLength(0); i++)
                Destroy(tuilesC[i]);
        }
        else if(PlayerPrefs.GetString ("mode") == "B"){
			tab = new char[tabB.GetLength(0), tabB.GetLength(1)];
			for (int ligne = 0;ligne < tabB.GetLength(0); ligne++ )
				for (int colonne = 0;colonne < tabB.GetLength(1); colonne++ )
					tab[colonne,ligne] = tabB[colonne,ligne];
            for (int i = 0; i < tuilesA.GetLength(0); i++)
                Destroy(tuilesA[i]);
            for (int i = 0; i < tuilesC.GetLength(0); i++)
                Destroy(tuilesC[i]);
        }
        else if (PlayerPrefs.GetString("mode") == "C")
        {
            tab = new char[tabC.GetLength(0), tabC.GetLength(1)];
            for (int ligne = 0; ligne < tabC.GetLength(0); ligne++)
                for (int colonne = 0; colonne < tabC.GetLength(1); colonne++)
                    tab[colonne, ligne] = tabC[colonne, ligne];
            for (int i = 0; i < tuilesA.GetLength(0); i++)
                Destroy(tuilesA[i]);
            for (int i = 0; i < tuilesB.GetLength(0); i++)
                Destroy(tuilesB[i]);
        }
    }

	private void randomization ()
	{
		int cpt = 0;

		while(cpt < 3000){
			int rnd = Random.Range(0,4);
			if (rnd == 0) deplacer ("N");
			if (rnd == 1) deplacer ("S");
			if (rnd == 2) deplacer ("E");
			if (rnd == 3) deplacer ("O");
			cpt++;
		}
		bRandomized = true;
	}

	private bool verification ()
	{
        if (tab[tab.GetLength(0) - 1, tab.GetLength(1) - 1] != ' ')
            return false;

		char lettre = 'A';
        string sLog = "";

		for (int ligne = 0; ligne < tab.GetLength(0); ligne++ ){
			for (int colonne = 0; colonne < tab.GetLength(1); colonne++ ){
                sLog += tab[ligne, colonne] +"";
				if(tab[ligne,colonne] != lettre++ && !(ligne == tab.GetLength(0) - 1 && colonne == tab.GetLength(1) - 1))
                    return false;
			}
            sLog += "\n";
		}
        Debug.Log(sLog);
		Debug.Log ("Gagné !");
		win.gameObject.GetComponent<Text> ().enabled = true;
        record.gameObject.GetComponent<Text>().enabled = true;
        nbDepl.GetComponent<Animator>().enabled = true;

        if(PlayerPrefs.GetString("mode") == "C")
        {
            if (PlayerPrefs.GetInt("nbDepl") < PlayerPrefs.GetInt("record3x3") || PlayerPrefs.GetInt("record3x3") == -1)
            {
                PlayerPrefs.SetInt("record3x3", PlayerPrefs.GetInt("nbDepl"));
                record.GetComponent<Text>().text = "Nouveau record : " + PlayerPrefs.GetInt("record3x3");
                record.GetComponent<Text>().color = new Color(255, 170, 0);
            }
            else
            {
                record.GetComponent<Text>().text = "Record : " + PlayerPrefs.GetInt("record3x3");
            }
        }
        else if (PlayerPrefs.GetString("mode") == "A")
        {
            if (PlayerPrefs.GetInt("nbDepl") < PlayerPrefs.GetInt("record4x4") || PlayerPrefs.GetInt("record4x4") == -1)
            {
                PlayerPrefs.SetInt("record4x4", PlayerPrefs.GetInt("nbDepl"));
                record.GetComponent<Text>().text = "Nouveau record : " + PlayerPrefs.GetInt("record4x4");
                record.GetComponent<Text>().color = new Color(255, 170, 0);
            }
            else
            {
                record.GetComponent<Text>().text = "Record : " + PlayerPrefs.GetInt("record4x4");
            }
        }
            
        else if (PlayerPrefs.GetString("mode") == "B")
        {
            if (PlayerPrefs.GetInt("nbDepl") < PlayerPrefs.GetInt("record5x5") || PlayerPrefs.GetInt("record5x5") == -1)
            {
                PlayerPrefs.SetInt("record5x5", PlayerPrefs.GetInt("nbDepl"));
                record.GetComponent<Text>().text = "Nouveau record : " + PlayerPrefs.GetInt("record5x5");
                record.GetComponent<Text>().color = new Color(255, 170, 0);
            }
            else
            {
                record.GetComponent<Text>().text = "Record : " + PlayerPrefs.GetInt("record5x5");
            }
        }
            
        up.SetActive (false);
		down.SetActive (false);
		left.SetActive (false);
		right.SetActive (false);

        bGagne = true;
		return true;
	}

	private void deplacer (string depl)
	{
		for (int ligne = 0;ligne < tab.GetLength(0); ligne++ ){
			for (int colonne = 0;colonne < tab.GetLength(1); colonne++ ){
				if(tab [ligne,colonne] == ' '){
					ligneCaseVide = ligne;
					colCaseVide = colonne;
				}
			}
		}
		
		if(depl.Equals("N") &&  (ligneCaseVide-1) >= 0){
			tab[ligneCaseVide,colCaseVide] = tab[ligneCaseVide -1 ,colCaseVide];
			tab[ligneCaseVide -1 ,colCaseVide] = ' ';
			if(bRandomized) PlayerPrefs.SetInt ("nbDepl", PlayerPrefs.GetInt ("nbDepl") + 1);
		}else if(depl.Equals("S") &&  (ligneCaseVide+1) < tab.GetLength(0)){
			tab[ligneCaseVide,colCaseVide] = tab[ligneCaseVide +1 ,colCaseVide];
			tab[ligneCaseVide +1 ,colCaseVide] = ' ';
			if(bRandomized) PlayerPrefs.SetInt ("nbDepl", PlayerPrefs.GetInt ("nbDepl") + 1);
		}else if(depl.Equals("O") &&  (colCaseVide-1) >= 0){
			tab[ligneCaseVide,colCaseVide] = tab[ligneCaseVide,colCaseVide -1];
			tab[ligneCaseVide,colCaseVide -1] = ' ';
			if(bRandomized) PlayerPrefs.SetInt ("nbDepl", PlayerPrefs.GetInt ("nbDepl") + 1);
		}else if(depl.Equals("E") &&  (colCaseVide+1) < tab.GetLength(0)){
			tab[ligneCaseVide,colCaseVide] = tab[ligneCaseVide,colCaseVide +1];
			tab[ligneCaseVide,colCaseVide +1] = ' ';
			if(bRandomized) PlayerPrefs.SetInt ("nbDepl", PlayerPrefs.GetInt ("nbDepl") + 1);
		}
		afficherTableau();
	}

	private void afficherTableau ()
	{
		for (int ligne = 0;ligne < tab.GetLength(0); ligne++ ){
			for (int colonne = 0;colonne < tab.GetLength(1); colonne++ ){
				transform.Find("Tuile" + PlayerPrefs.GetString ("mode") + ligne.ToString() + colonne.ToString()).gameObject.GetComponentInChildren<Text>().text = tab[colonne,ligne].ToString();
			}
		}	
	}
}
