﻿using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour {

    public Sprite son;
    public Sprite son2;
    public Sprite main;
    public Sprite main2;

    private Vector3 taille;

    // Use this for initialization
    void Start () {
        if (! PlayerPrefs.HasKey("commandes")) PlayerPrefs.SetString("commandes", "click");
        if (! PlayerPrefs.HasKey("son")) PlayerPrefs.SetInt("son", 1);
        if (! PlayerPrefs.HasKey("record3x3")) PlayerPrefs.SetInt("record3x3", -1);
        if (! PlayerPrefs.HasKey("record4x4")) PlayerPrefs.SetInt("record4x4", -1);
        if (! PlayerPrefs.HasKey("record5x5")) PlayerPrefs.SetInt("record5x5", -1);

        taille = this.transform.localScale;

        if (PlayerPrefs.GetString("commandes") == "swipe")
            GameObject.Find("Mode").transform.Find("Mode_2").transform.Find("main").GetComponent<SpriteRenderer>().sprite = main;

        if (PlayerPrefs.GetInt("son") == 0)
            GameObject.Find("Son").transform.Find("Son_2").transform.Find("son").GetComponent<SpriteRenderer>().sprite = son2;
    }

    void OnMouseDown()
    {
        this.transform.localScale = new Vector3(taille.x / 1.1f, taille.y / 1.1f, taille.z / 1.1f);
    }

    void OnMouseExit()
    {
        this.transform.localScale = new Vector3(taille.x, taille.y, taille.z);
    }

    void OnMouseUpAsButton()
    {
        this.transform.localScale = new Vector3(taille.x, taille.y, taille.z);

        if (this.name.Equals("Normal"))
        {
            PlayerPrefs.SetString("mode", "A");
            Application.LoadLevel("load");
        }
        if (this.name.Equals("Difficile"))
        {
            PlayerPrefs.SetString("mode", "B");
            Application.LoadLevel("load");
        }
        if (this.name.Equals("Facile"))
        {
            PlayerPrefs.SetString("mode", "C");
            Application.LoadLevel("load");
        }

        if (this.name.Equals("Mode"))
        {
            if (PlayerPrefs.GetString("commandes").Equals("swipe"))
            {
                GameObject.Find("Mode").transform.Find("Mode_2").transform.Find("main").GetComponent<SpriteRenderer>().sprite = main2;
                PlayerPrefs.SetString("commandes", "click");
            }
            else if (PlayerPrefs.GetString("commandes").Equals("click"))
            {
                GameObject.Find("Mode").transform.Find("Mode_2").transform.Find("main").GetComponent<SpriteRenderer>().sprite = main;
                PlayerPrefs.SetString("commandes", "swipe");
            }
        }

        if (this.name.Equals("Son"))
        {
            if (PlayerPrefs.GetInt("son") == 0)
            {
                this.transform.Find("Son_2").transform.Find("son").GetComponent<SpriteRenderer>().sprite = son;
                PlayerPrefs.SetInt("son", 1);
            }
            else if (PlayerPrefs.GetInt("son") == 1)
            {
                this.transform.Find("Son_2").transform.Find("son").GetComponent<SpriteRenderer>().sprite = son2;
                PlayerPrefs.SetInt("son", 0);
            }
        }
    }
}
