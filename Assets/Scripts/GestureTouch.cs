﻿using UnityEngine;
using System.Collections;

public class GestureTouch : MonoBehaviour {

    public float minSwipeDistY;
    public float minSwipeDistX;

    private Vector2 startPos;

    void Update()
    {
        if (PlayerPrefs.GetString("commandes") != "swipe")
            return;

        //#if UNITY_ANDROID
        if (Input.touchCount > 0)
        {
            Touch touch = Input.touches[0];

            switch (touch.phase)
            {
                case TouchPhase.Began:
                    startPos = touch.position;
                    break;

                case TouchPhase.Ended:
                    float swipeDistVertical = (new Vector3(0, touch.position.y, 0) - new Vector3(0, startPos.y, 0)).magnitude;

                    if (swipeDistVertical > minSwipeDistY)
                    {
                        float swipeValue = Mathf.Sign(touch.position.y - startPos.y);
                        if (swipeValue > 0) //up swipe
                        {
                            PlayerPrefs.SetString("direction", "S");
                        }
                        else if (swipeValue < 0) //down swipe
                        {
                            PlayerPrefs.SetString("direction", "N");
                        }
                    }

                    float swipeDistHorizontal = (new Vector3(touch.position.x, 0, 0) - new Vector3(startPos.x, 0, 0)).magnitude;

                    if (swipeDistHorizontal > minSwipeDistX)
                    {
                        float swipeValue = Mathf.Sign(touch.position.x - startPos.x);

                        if (swipeValue > 0) //right swipe
                        {
                            PlayerPrefs.SetString("direction", "O");
                        }
                        else if (swipeValue < 0) //left swipe
                        {
                            PlayerPrefs.SetString("direction", "E");
                        }
                    }
                    break;
            }
        }
    }
}
