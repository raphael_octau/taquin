﻿using UnityEngine;
using System.Collections;

public class direction : MonoBehaviour {

	private Vector3 taille;

	// Use this for initialization
	void Start () {
		taille = this.transform.localScale;
	}

	void OnMouseDown ()
	{
		this.transform.localScale = new Vector3 (taille.x / 1.1f, taille.y / 1.1f, taille.z / 1.1f);
	}

	
	void OnMouseExit ()
	{
		this.transform.localScale = new Vector3 (taille.x, taille.y, taille.z);
	}

	// Update is called once per frame
	void OnMouseUpAsButton () {
        if(PlayerPrefs.GetString("commandes") == "int")
        {
            if (this.gameObject.name.Equals("Right")) PlayerPrefs.SetString("direction", "E");
            if (this.gameObject.name.Equals("Left")) PlayerPrefs.SetString("direction", "O");
            if (this.gameObject.name.Equals("Up")) PlayerPrefs.SetString("direction", "N");
            if (this.gameObject.name.Equals("Down")) PlayerPrefs.SetString("direction", "S");
        }
        else
        {
            if (this.gameObject.name.Equals("Right")) PlayerPrefs.SetString("direction", "O");
            if (this.gameObject.name.Equals("Left")) PlayerPrefs.SetString("direction", "E");
            if (this.gameObject.name.Equals("Up")) PlayerPrefs.SetString("direction", "S");
            if (this.gameObject.name.Equals("Down")) PlayerPrefs.SetString("direction", "N");
        }

		this.transform.localScale = new Vector3 (taille.x, taille.y, taille.z);
	}
}
