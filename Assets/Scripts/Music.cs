﻿using UnityEngine;
using System.Collections;

public class Music : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (PlayerPrefs.GetInt("son") == 0)
            this.GetComponent<AudioSource>().Stop();
        else if(!this.GetComponent<AudioSource>().isPlaying)
            this.GetComponent<AudioSource>().Play();
    }

    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);

        if (FindObjectsOfType(GetType()).Length > 1)
        {
            Destroy(this.gameObject);
        }
    }
}
